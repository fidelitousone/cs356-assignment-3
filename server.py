#Name: Jatinder Singh
#UCID: js843
#Section: 001


import socket
import datetime,time
import pytz
import os.path
import sys
import re
import math

HOST = "localhost"
PORT = 12001

HTTP_VERSION = "HTTP/1.1"

HTTP_OK = "200"
HTTP_NOT_FOUND = "404"
HTTP_NOT_MODIFIED = "304"

condex = re.compile("If-Modified-Since:")

def parse_http(message_data):

    if (condex.search(message_data) is not None):
        cond_get = message_data.split("If-Modified-Since: ")
        cond_get = cond_get[1]
    else:
        cond_get = False
    
    http_array = message_data.split()

    method = http_array[0]
    file_obj = http_array[1].strip("/")
    return (method,file_obj, cond_get)

    
def not_found():
    t = datetime.datetime.now(tz=pytz.UTC)
    plswork = t.strftime("%a, %d %b %Y %H:%M:%S %Z")
    response = ""
    response += HTTP_VERSION + " " + HTTP_NOT_FOUND
    response += " " + "Not Found" + "\r\n"
    response += "Date: " + plswork  + "\r\n"
    response += "\r\n"
    return response

def get(content_size, content, last_modified):
    t = datetime.datetime.now(tz=pytz.UTC)
    plswork = t.strftime("%a, %d %b %Y %H:%M:%S %Z")
    response = ""
    response += HTTP_VERSION + " " + HTTP_OK
    response += " " + "OK" + "\r\n"
    response += "Date: " + plswork  + "\r\n"
    response += "Last-Modified: " + str(last_modified) + "\r\n"
    response += "Content-Length: " + str(content_size) + "\r\n"
    response += "Content-Type: text/html; charset=UTF-8\r\n"
    response += "\r\n"
    response += str(content)
    return response

def get_cond():
    t = datetime.datetime.now(tz=pytz.UTC)
    plswork = t.strftime("%a, %d %b %Y %H:%M:%S %Z")

    response = ""
    response += HTTP_VERSION + " " +  HTTP_NOT_MODIFIED
    response += " " + "Not Modified" + "\r\n"
    response += "Date: " + plswork + "\r\n"
    return response

def web_object_info(obj_name):
    
    with open(obj_name, "r") as file_thing:
        content = file_thing.read().replace('\n','')
        
    content_size = len(content)
    
    last_mod = os.path.getmtime(obj_name)

    t = time.gmtime(last_mod)

    last_mod = time.strftime("%a, %d %b %Y %H:%M:%S GMT", t)

    print(str(last_mod))
    return (content, content_size, last_mod)

def handle_request(http_headers):

    METHOD = http_headers[0]
    WEB_OBJ = http_headers[1]
    LAST_MOD = http_headers[2]
    
    if METHOD == "GET":
        
        if (not LAST_MOD):
            #Normal get
            if os.path.isfile(WEB_OBJ):
                obj_info = web_object_info(WEB_OBJ)
                response = get(obj_info[1], obj_info[0], obj_info[2])
            else:
                # Returning a 404 to the client.
                response = not_found()
        else:
            #cache get
            if (os.path.isfile(WEB_OBJ)):
                acttime = LAST_MOD.strip("\r\n")
                ugh = time.strptime(acttime, "%a, %d %b %Y %H:%M:%S %Z")
                secs = time.mktime(ugh)
            
                obj_info = web_object_info(WEB_OBJ)
            
                please3 = time.strptime(obj_info[2], "%a, %d %b %Y %H:%M:%S %Z")

                secondstest = time.mktime(please3)
            
            
                if (secondstest == secs):
                    response = get_cond()
                else:
                
                    obj_info = web_object_info(WEB_OBJ)
                
                    seconds = os.path.getmtime(WEB_OBJ)
                    PLEASE1 = time.gmtime(seconds)
                    PLEASE = time.strftime("%a, %d %b %Y %H:%M:%S GMT", PLEASE1)
                    response = get(obj_info[1], obj_info[0], PLEASE)
            else:
                response = not_found()


    else:
        # This probably should implement a "Not implemented method" but fuck it ill do it if I have time.
        
        print("Unknown HTTP protocol")
        sys.exit(2)
    return response
    

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind((HOST, PORT))
server.listen(2)
conn, addr = server.accept()




data = conn.recv(1024)
    
message_dat = data.decode()

print(message_dat)
headers  = parse_http(message_dat)


yeet = handle_request(headers)


print(yeet)

# Send back the response to the client.
conn.sendall(yeet.encode())
