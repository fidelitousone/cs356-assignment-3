#Name: Jatinder Singh
#UCID: js843
#Section: 001

import sys
import re
import socket
import getopt
import logging
import os.path


HTTP_VERSION = "HTTP/1.1"
cond_get_re = re.compile("Last-Modified:")

def cache_exists():
    if os.path.isfile("cache.txt"):
        return True
    else:
        return False

def http_get(hostname, port, web_object):
    request_header = ""

    request_header += "GET "
    request_header += "/" + web_object + " "
    request_header += HTTP_VERSION + "\r\n"
    
    request_header += "Host: " + hostname + ":" + port + "\r\n"
    request_header += "\r\n"
    return request_header
    

def http_cond_get(hostname, port, web_object, last_mod):
    request_header = ""

    request_header += "GET "
    request_header += "/" + web_object + " "
    request_header += HTTP_VERSION + "\r\n"
    
    request_header += "Host: " + hostname + ":" + port + "\r\n"
    request_header += "If-Modified-Since: " + last_mod + "\r\n"
    request_header += "\r\n"
    return request_header
    
    
def check_url(url):
    url_regex = re.compile("[a-z]+:[0-9]+\/.+.html")
    
    if (url_regex.fullmatch(url) is None):
        return False
    else:
        return True
    


try:
    options, args = getopt.getopt(sys.argv[1:], "d",["debug"])
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)
    
for option, argument in options:
    if option in ("-d", "--debug"):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        print("logging enabled")
    else:
        assert False, "unhandled option"
        

#TODO: A Regular expression should go here to detect the url just in case my debug flag is out of order
url = ""
for argument in sys.argv:
    if check_url(argument) == True:
        url = argument
        break
    else:
        continue


#Test lines for cache fnc
#if cache_exists():
#    print("cache detected")
#else:
#    print("cache not detected.")

    
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#The shit that goes here is the program
    

split_url = url.split(':')

hostname = split_url[0]

logging.debug("Hostname: " + hostname)
    
split_url_slash = split_url[1].split('/')



port = split_url_slash[0]

logging.debug("Port: " + str(port))

web_object = split_url_slash[1]

logging.debug("File to get: " + web_object)


destination = (hostname, int(port))

client.connect(destination)
  
if cache_exists():
    # This needs to send the condtional get because the cache already exists
    
    cachef = open("cache.txt", "r")
    
    for line in cachef:
        if (cond_get_re.search(line) is None):
            continue
        else:
            moditime = line.split(": ")
            
    moditime = moditime[1].strip("\n")
    req = http_cond_get(hostname, str(port), web_object, moditime)
    client.sendall(req.encode())

    raw_net_dat = client.recv(1024)
    
    headerinfo = raw_net_dat.decode()
    
    headerinfo1 = headerinfo.split()
    
    if (headerinfo1[1] == "304"):
        print("Header Info:\n")
        print(headerinfo)
        f = open("cache.txt", "r+")
        cache_local = f.read()
        print("Cache Info:\n")
        print(cache_local)
    elif (headerinfo1[1] == "200"):
        f = open("cache.txt", "r+")
        f.truncate(0)
        print(headerinfo + "\n")
        f.write(headerinfo)
        f.close()
        d = open("cache.txt", "r+")
        dataex = d.read()
        print("new cached info\n" + dataex)
    else:
        print("header info:\n")
        print(headerinfo)
        
else:
    # This needs to send a fresh http request and cache it if it exist
    req = http_get(hostname, str(port), web_object)
    client.sendall(req.encode())
    raw_net_data = client.recv(1024)

    data = raw_net_data.decode()

    #this exists only because I need a way to parse the req
    data1 = data.split()
    
    #TODO: This needs to not write the damn file if it's a 404 request!!!!!

    print(data)

    if (data1[1] == "404"):
        print("404, cache wasn't written")
    else:
        with open("cache.txt", "w+") as f:
            f.write(data)
    
